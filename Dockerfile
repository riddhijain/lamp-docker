FROM ubuntu:latest

RUN apt-get update -y && apt-get install -y nginx && apt-get install -y php-fpm  && apt-get install nano

RUN cd /etc/nginx/sites-enabled && sed -i 's/index index.html/index index.php index.html/g' default && sed -i '/location.*php/s/#//' default && sed -i '/location.*php/{n;s/#//;}' default && sed -i '/php7.4-fpm.sock/s/#//' default && sed -i  '/php7.4-fpm.sock/{n;n;n;s/#//;}' default

WORKDIR /var/www/html
RUN echo "<?php echo 'Hello from php' ?>" >index.php && echo "service nginx start" >> /etc/entrypoint.sh && echo "service php7.4-fpm start" >>/etc/entrypoint.sh && echo "php index.php" >>/etc/entrypoint.sh && echo "tail -f /dev/null" >>/etc/entrypoint.sh 


EXPOSE 80

ENTRYPOINT ["sh", "/etc/entrypoint.sh"]

